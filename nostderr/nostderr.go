package nostderr

import (
	"errors"
	"io"
	"io/ioutil"
)

var write = ioutil.Discard
var clone io.Writer
var killstderr func() error

// SetWriter sets the writer for stderr instead of ioutil.Discard
func SetWriter(writer io.Writer) {
	write = writer
}

// SetClone sets the clone writer for stderr
func SetClone(writer io.Writer) {
	clone = writer
}

// ErrUnsupportedPlatform is returned when the platform is unsupported.
var ErrUnsupportedPlatform = errors.New("Unsupported platform")

func Start() error {
	if killstderr == nil {
		return ErrUnsupportedPlatform
	}

	return killstderr()
}

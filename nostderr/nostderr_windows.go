package nostderr

import (
	"io"
	"os"
	"syscall"
)

var (
	kernel32         = syscall.MustLoadDLL("kernel32.dll")
	procSetStdHandle = kernel32.MustFindProc("SetStdHandle")
)

func setStdHandle(stdhandle int32, handle syscall.Handle) error {
	r0, _, e1 := syscall.Syscall(procSetStdHandle.Addr(), 2, uintptr(stdhandle), uintptr(handle), 0)
	if r0 == 0 {
		if e1 != 0 {
			return error(e1)
		}

		return syscall.EINVAL
	}

	return nil
}

func init() {
	killstderr = func() error {
		r, w, err := os.Pipe()
		if err != nil {
			return err
		}

		go io.Copy(write, r)
		if clone != nil {
			go io.Copy(clone, r)
		}

		os.Stderr = w

		return setStdHandle(
			syscall.STD_ERROR_HANDLE,
			syscall.Handle(w.Fd()),
		)
	}
}

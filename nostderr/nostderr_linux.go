package nostderr

import (
	"io"
	"os"
	"syscall"
)

func init() {
	killstderr = func() error {
		r, w, err := os.Pipe()
		if err != nil {
			return err
		}

		go io.Copy(write, r)
		if clone != nil {
			go io.Copy(clone, r)
		}

		os.Stderr = w

		return syscall.Dup3(int(w.Fd()), 2, 0)
	}
}

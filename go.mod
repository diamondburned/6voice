module gitlab.com/diamondburned/6voice

go 1.12

require (
	github.com/bwmarrin/dgvoice v0.0.0-20170706020935-3c939eca8b2f
	github.com/bwmarrin/discordgo v0.16.1-0.20190528235223-789616715332
	github.com/davecgh/go-spew v1.1.0
	github.com/diamondburned/discordgo v1.1.2
	github.com/diamondburned/tcell v1.1.7-0.20190608162241-468b8880ec7a
	github.com/diamondburned/tview v1.2.2-0.20190609100116-f44377c613dc
	github.com/gdamore/tcell v1.1.2
	github.com/gordonklaus/portaudio v0.0.0-20180817120803-00e7307ccd93
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/rivo/tview v0.0.0-20190602193159-384f577a620b
	golang.org/x/crypto v0.0.0-20190611184440-5c40567a22f8 // indirect
	golang.org/x/net v0.0.0-20190611141213-3f473d35a33a // indirect
	golang.org/x/sys v0.0.0-20190610200419-93c9922d18ae // indirect
	golang.org/x/tools v0.0.0-20190611222205-d73e1c7e250b // indirect
	layeh.com/gopus v0.0.0-20161224163843-0ebf989153aa // indirect
)

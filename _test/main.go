package main

import (
	"os"
	"os/signal"

	"github.com/bwmarrin/discordgo"
	"github.com/k0kubun/pp"
)

func main() {
	d, err := discordgo.New(os.Getenv("TOKEN"))
	if err != nil {
		panic(err)
	}

	ready := make(chan struct{})

	d.AddHandler(func(_ *discordgo.Session, r *discordgo.Ready) {
		ready <- struct{}{}
	})

	d.AddHandler(func(_ *discordgo.Session, v *discordgo.VoiceStateUpdate) {
		if v.GuildID != "361910177961738242" {
			return
		}

		pp.Println(v)
	})

	if err := d.Open(); err != nil {
		panic(err)
	}

	defer d.Close()

	<-ready

	g, err := d.State.Guild("361910177961738242")
	if err != nil {
		panic(err)
	}

	pp.Println(g.VoiceStates)

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt)
	<-sig
}

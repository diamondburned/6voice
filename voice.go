package main

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/6voice/voice"
	"gitlab.com/diamondburned/6voice/voiceview"
)

func joinVoice(s *discordgo.Session, vv *voiceview.VoiceView, ctx *voice.Context) func(*discordgo.Channel) {
	return func(ch *discordgo.Channel) {
		go func() {
			if err := ctx.JoinChannel(ch); err != nil {
				log.Fatalln(err)
				return
			}

			if err := vv.SetVoiceConn(ctx.Voice); err != nil {
				log.Fatalln(err)
			}
		}()
	}
}

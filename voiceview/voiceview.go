package voiceview

import (
	"log"
	"sort"
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/diamondburned/tcell"
	"github.com/diamondburned/tview"
	"gitlab.com/diamondburned/6voice/primitives"
)

type VoiceView struct {
	*tview.List
	text *primitives.CenterText

	ses     *discordgo.Session
	channel *discordgo.Channel
	guild   *discordgo.Guild

	conn *discordgo.VoiceConnection

	mu     sync.Mutex
	states map[string]*VoiceViewItem

	// Temporary fields that should only be used during Draw
	// items []*tview.ListItem
}

func New(ses *discordgo.Session) *VoiceView {
	list := tview.NewList()
	ct := primitives.NewCenterText()
	ct.SetText("Placeholder")

	v := &VoiceView{
		List:   list,
		text:   ct,
		ses:    ses,
		states: map[string]*VoiceViewItem{},
	}

	ses.AddHandler(v.getStateUpdateHandler())

	return v
}

func (v *VoiceView) SetVoiceConn(vc *discordgo.VoiceConnection) error {
	v.conn = vc

	if v.channel == nil || v.channel.ID != vc.ChannelID {
		c, err := v.ses.State.Channel(vc.ChannelID)
		if err != nil {
			c, err = v.ses.Channel(vc.ChannelID)
		}

		if err != nil {
			return err
		}

		v.channel = c
	}

	if v.guild == nil || v.guild.ID != vc.GuildID {
		g, err := v.ses.State.Guild(vc.GuildID)
		if err != nil {
			g, err = v.ses.Guild(vc.GuildID)
		}

		if err != nil {
			return err
		}

		v.guild = g
	}

	// Add the Speaking Update handler into session
	v.conn.AddHandler(v.getSpeakingUpdateHandler())

	return nil
}

func (v *VoiceView) isInVoice() bool {
	return !(v.channel == nil || v.guild == nil || v.conn == nil)
}

func (v *VoiceView) getSpeakingUpdateHandler() discordgo.VoiceSpeakingUpdateHandler {
	return func(_ *discordgo.VoiceConnection, vsu *discordgo.VoiceSpeakingUpdate) {
		if !v.isInVoice() {
			return
		}

		v.mu.Lock()
		defer v.mu.Unlock()

		st, ok := v.states[vsu.UserID]
		if !ok {
			return
		}

		if err := st.ReflectSpeaking(vsu); err != nil {
			log.Println(err)
		}

		tview.ExecApplication(func(app *tview.Application) bool {
			return true
		})
	}
}

func (v *VoiceView) getStateUpdateHandler() func(s *discordgo.Session, vsu *discordgo.VoiceStateUpdate) {
	return func(_ *discordgo.Session, vsu *discordgo.VoiceStateUpdate) {
		if !v.isInVoice() {
			return
		}

		v.mu.Lock()
		defer v.mu.Unlock()

		// ChannelID is empty when a user has left a voice channel
		if vsu.ChannelID == "" {
			// We check if the user is in our list of voice chat, then remove them
			if _, ok := v.states[vsu.UserID]; ok {
				delete(v.states, vsu.UserID)
			}
		} else {
			// If the update event is not ours
			if vsu.ChannelID != v.channel.ID {
				return
			}

			it, ok := v.states[vsu.UserID]
			if !ok {
				// Make a new entry
				i, err := v.NewVoiceViewItem(vsu.VoiceState)
				if err != nil {
					log.Println(err)
					return
				}

				v.states[vsu.UserID] = i

				it = i
			}

			it.ReflectNewState(vsu.VoiceState)
		}

		tview.ExecApplication(func(app *tview.Application) bool {
			return true
		})
	}
}

func (v *VoiceView) Draw(s tcell.Screen) {
	if v.conn == nil {
		v.text.SetRect(v.List.GetRect())
		v.text.SetText("Currently not in voice")
		v.text.Draw(s)
	} else if len(v.states) == 0 {
		v.text.SetRect(v.List.GetRect())
		v.text.SetText("Nobody is in voice")
		v.text.Draw(s)
	} else {
		items := make([]*VoiceViewItem, 0, len(v.states))
		for _, s := range v.states {
			items = append(items, s)
		}

		sort.Slice(items, func(i, j int) bool {
			return items[i].name < items[i].name
		})

		// Clear the list
		v.List.Items = v.List.Items[:0]

		for _, i := range items {
			v.List.Items = append(v.List.Items, &i.ListItem)
		}

		v.List.Draw(s)
	}
}

package primitives

import (
	"github.com/diamondburned/tcell"
	"github.com/diamondburned/tview"
)

// var _ tview.Primitive = &CenterText{}

type CenterText struct {
	x, y          int
	width, height int
	focus         tview.Focusable

	s []rune
}

// NewCenterText makes a new loading struct. This only
// does one line.
func NewCenterText() *CenterText {
	return &CenterText{}
}

// SetText sets the text
func (l *CenterText) SetText(text string) {
	l.s = []rune(text)
}

// Draw draws
func (l *CenterText) Draw(s tcell.Screen) {
	if l.width <= 0 || l.height <= 0 {
		return
	}

	x, y := l.x+l.width/2-len(l.s)/2, l.y+l.height/2

	s.ShowCursor(x, y)

	for i := 0; i < len(l.s); i++ {
		s.SetContent(x+i, y, l.s[i], nil, 0)
	}

	s.HideCursor()
}

// GetRect returns the rectangle dimensions
func (l *CenterText) GetRect() (int, int, int, int) {
	return l.x, l.y, l.width, l.height
}

// SetRect sets the rectangle dimensions
func (l *CenterText) SetRect(x, y, width, height int) {
	l.x = x
	l.y = y
	l.width = width
	l.height = height
}

// InputHandler sets no input handler, satisfying Primitive
func (l *CenterText) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return nil
}

// Focus does nothing, really.
func (*CenterText) Focus(delegate func(tview.Primitive)) {}

// Blur also does nothing.
func (*CenterText) Blur() {}

// HasFocus always returns false, as you can't focus on this.
func (*CenterText) HasFocus() bool {
	return false
}

// GetFocusable does whatever the fuck I have no idea
func (l *CenterText) GetFocusable() tview.Focusable {
	return l.focus
}

func (*CenterText) SetOnFocus(func()) {}
func (*CenterText) SetOnBlur(func())  {}

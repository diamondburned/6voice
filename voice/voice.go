package voice

import (
	"fmt"

	"github.com/bwmarrin/dgvoice"
	"github.com/bwmarrin/discordgo"
	"github.com/gordonklaus/portaudio"
)

const (
	// SampleCount is just magic number shit
	// https://opus-codec.org/docs/html_api-1.0.1/group__opus__encoder.html
	SampleCount = 960

	// Channels is the channel count
	Channels = 2

	bufSize = 960 * 2
)

// silence
var empty = [1920]int16{}

// Context declares the voice context. Context is re-usable,
// for maintaining one connection at a time.
type Context struct {
	stopSend chan struct{}
	stopRecv chan struct{}

	ses   *discordgo.Session
	Voice *discordgo.VoiceConnection

	outputStream *portaudio.Stream
	inputStream  *portaudio.Stream

	// ErrHandler is the error handler, used for anything that errors in the
	// goroutines or the Close method. This function will block the thread,
	// so you should put it in a goroutine if it blocks.
	ErrHandler func(error)
}

// New creates a new voice context. This does not initialize PortAudio by
// itself.
func New(s *discordgo.Session) *Context {
	ctx := &Context{
		stopSend:   make(chan struct{}),
		stopRecv:   make(chan struct{}),
		ses:        s,
		ErrHandler: func(error) {},
	}

	return ctx
}

// JoinChannel joins a voice channel. This calls the leave method
// automatically.
func (ctx *Context) JoinChannel(ch *discordgo.Channel) error {
	if ch.Type != discordgo.ChannelTypeGuildVoice {
		return fmt.Errorf("Invalid/unsupported channel type: %d", ch.Type)
	}

	if err := ctx.LeaveChannel(); err != nil {
		return err
	}

	// Start joining the voice channel
	v, err := ctx.ses.ChannelVoiceJoin(ch.GuildID, ch.ID, false, false)
	if err != nil {
		return fmt.Errorf("Failed joining channel: %s", err.Error())
	}

	ctx.Voice = v

	// Tell Discord to start sending audio
	ctx.ToggleSpeaking(true)

	if err := ctx.startRecv(); err != nil {
		// Gracefully leave on error
		ctx.Voice.Disconnect()
		return fmt.Errorf("Failed receiving audio: %s", err.Error())
	}

	if err := ctx.startSend(); err != nil {
		ctx.Voice.Disconnect()
		return fmt.Errorf("Failed sending audio: %s", err.Error())
	}

	return nil
}

// ToggleSpeaking toggles speaking
func (ctx *Context) ToggleSpeaking(b bool) {
	if b {
		ctx.Voice.Speaking(true)
		ctx.Voice.OpusSend <- []byte{0xF8, 0xFF, 0xFE}
	} else {
		ctx.Voice.Speaking(false)
	}
}

// LeaveChannel leaves the channel and stops the audio contexts if in one.
func (ctx *Context) LeaveChannel() error {
	if ctx.Voice != nil {
		if err := ctx.Voice.Disconnect(); err != nil {
			return err
		}

		// If VC is not nil, obviously the goroutines would've started,
		// making it safe to just pass in the empty structs
		ctx.stopSend <- struct{}{}
		ctx.stopRecv <- struct{}{}
	}

	return nil
}

// Close closes the context, freeing up resources and safely disconnecting
// from Discord.
func (ctx *Context) Close() {
	// Close the Voice connection
	if ctx.Voice != nil {
		if err := ctx.Voice.Disconnect(); err != nil {
			ctx.ErrHandler(err)
		}
	}

	// Close the PortAudio streams
	close(ctx.stopSend)
	close(ctx.stopRecv)

	if err := ctx.inputStream.Close(); err != nil {
		ctx.ErrHandler(err)
	}

	if err := ctx.outputStream.Close(); err != nil {
		ctx.ErrHandler(err)
	}

	return
}

func (ctx *Context) startSend() error {
	// Create the input slice
	in := make([]int16, bufSize)

	if ctx.inputStream == nil {
		s, err := portaudio.OpenDefaultStream(2, 0, 48000, bufSize, &in)
		if err != nil {
			return err
		}

		ctx.inputStream = s
	}

	if err := ctx.inputStream.Start(); err != nil {
		return err
	}

	buf := make(chan []int16)
	go dgvoice.SendPCM(ctx.Voice, buf)

	go func() {
	Loop:
		for {
			select {
			case <-ctx.stopSend:
				break Loop
			case buf <- in:
			}

			if err := ctx.inputStream.Read(); err != nil {
				ctx.ErrHandler(err)
			}
		}

		if err := ctx.inputStream.Stop(); err != nil {
			ctx.ErrHandler(err)
		}

		close(buf)
	}()

	return nil
}

func (ctx *Context) startRecv() error {
	// Create the output slice
	out := make([]int16, bufSize)

	// Initiate the output stream and start it
	if ctx.outputStream == nil {
		s, err := portaudio.OpenDefaultStream(0, 2, 48000, bufSize, &out)
		if err != nil {
			return err
		}

		ctx.outputStream = s
	}

	if err := ctx.outputStream.Start(); err != nil {
		return err
	}

	recv := make(chan *discordgo.Packet, 46)
	go dgvoice.ReceivePCM(ctx.Voice, recv)

	go func() {
	Loop:
		for {
			select {
			case i := <-recv:
				*&out = i.PCM
			case _ = <-ctx.stopRecv:
				break Loop
			default:
				*&out = empty[:]
			}

			if err := ctx.outputStream.Write(); err != nil {
				ctx.ErrHandler(err)
			}
		}

		if err := ctx.outputStream.Stop(); err != nil {
			ctx.ErrHandler(err)
		}

		close(recv)
	}()

	return nil
}
